/*
 * Author:  Dustin Haring
 * Date:    May 21, 2018
 * Comment: This file contains the definitions for the state machine class
 * 
 * Modified By: n/a
 * Date:        n/a
 * Comment:     n/a
 */

#include "stateMachine.h"

/** functType is a function pointer of your derived class with parameter EventData*.
 * ClassType is simply the derived class type ie TestClass.  
 * 
 * In derived class, State/IEvent nor EEvent functions may be called outside of the state machine.
 * Always use the raiseEvent() functions to call another one of these functions to prevent resource
 * access problems among mutiple threads.*/
template <class functType, class ClassType>
StateMachine<functType,ClassType>::StateMachine(ClassType* ClassInstance, unsigned int Max_States, unsigned int Max_IEvents, unsigned int Max_EEvents, functType nullFunct) : _maxStates(Max_States),
                                                                                               _maxIEvents(Max_IEvents), _maxEEvents(Max_EEvents)
{
    if (nullFunct == NULL)
    {
        throw;
    }

    /* create 2d dynamic array of type functType to be the state/event map */
    stateIEventMap = new stateEventStruct *[_maxStates];
    for (unsigned int i = 0; i < _maxIEvents; i++)
    {
        /* create and initialize the stateEventFunctMap variable with the passed nullFunct */
        stateIEventMap[i] = new stateEventStruct[_maxIEvents]{nullFunct}; //this method of initialization is only available in -std=c++11
    }

    /* create 1D array of External Event to function map */
    //EEventFunctMap = new functType[_maxEEvents]{nullFunct}; //this generates an internal compiler error. this method of initialization is only available in -std=c++11
    EEventFunctMap = new functType[_maxEEvents]; //this method of initialization is only available in -std=c++11; 
    //must use the for loop for initialization due to internal compiler error.
    for (unsigned int i = 0; i < _maxEEvents; i++)
    {
        EEventFunctMap[i] = nullFunct;
    }

    //save the instance of our derived class
    _classInstance = ClassInstance;
}

template <class functType, class ClassType>
StateMachine<functType,ClassType>::~StateMachine()
{
    for (unsigned long i = 0; i < _maxStates; ++i)
    {
        delete[] stateIEventMap[i]; //deletes an inner array of integer;
    }
    delete[] stateIEventMap; //delete pointer holding array of pointers;
}

template <class functType, class ClassType>
void StateMachine<functType,ClassType>::map(unsigned int state, unsigned int IEvent, unsigned int newState, functType Funct)
{
    if (0 >= state < _maxStates && 0 >= IEvent < _maxIEvents)
    {
        stateIEventMap[state][IEvent] = stateEventStruct(newState, Funct);
    }
    else
    {
        throw;//("StateMachine<functType,ClassType>::map state or IEvent is out of bounds.\n");
    }
}

template <class functType, class ClassType>
void StateMachine<functType,ClassType>::map(unsigned int state, unsigned int IEvent, functType Funct)
{
    if (0 >= state < _maxStates && 0 >= IEvent < _maxIEvents)
    {
        stateIEventMap[state][IEvent] = stateEventStruct(state, Funct);
    }
    else
    {
        throw;//("StateMachine<functType,ClassType>::map state or IEvent is out of bounds.\n");
    }
}

template <class functType, class ClassType>
void StateMachine<functType,ClassType>::map(unsigned int EEvent, functType Funct)
{
    if (0 >= EEvent < _maxIEvents)
    {
        EEventFunctMap[EEvent] = Funct;
    }
    else
    {
        throw;//("StateMachine<functType,ClassType>::map EEvent is out of bounds.\n");
    }
}

template <class functType, class ClassType>
void StateMachine<functType,ClassType>::raiseEEvent(unsigned int EEvent)
{
    if (_maxEEvents < EEvent || EEvent < 0)
    {
        throw;//("StateMachine<functType,ClassType>::raiseEEvent invalid EEvent passed. Out of bounds.\n");
    }

    EventData* data = EVENT_QUEUE.front().eData;

    accessControl.lock(); //obtain lock on function to make the following call thread safe.
    (_classInstance->*EEventFunctMap[EEvent])(data);//up to this function to generate internal event if necessary.
    accessControl.unlock(); //release lock
    
    delete data;

    accessControl.lock(); //obtain lock on function to make the following call thread safe.

    /** call the state engine with from our parent class's instance. */
    _classInstance->StateEngine();  

    accessControl.unlock(); //release lock
}

template <class functType, class ClassType>
void StateMachine<functType,ClassType>::raiseIEvent(unsigned int IEvent, EventData* eData)
{
    if (_maxIEvents < IEvent || IEvent < 0)
    {
        throw;//("StateMachine<functType,ClassType>::raiseEEvent invalid EEvent passed. Out of bounds.\n");
    }

    EVENT_QUEUE.push(EVENT_STRUCT(IEvent,eData));
}

template <class functType, class ClassType>
void StateMachine<functType,ClassType>::StateEngine()
{

    while(EVENT_QUEUE.empty() == false)
    {
        if (0 < EVENT_QUEUE.front().event >= _maxIEvents)
        {
            throw;//("StateMachine<T,ClassType>::StateEngine Event in EVENT_QUEUE is out of bounds.\n");
        }

        EventData* data = EVENT_QUEUE.front().eData;
        // execute the state passing in event data, if any
        (_classInstance->*stateIEventMap[_currentState][EVENT_QUEUE.front().event].funct)(data);
        
        delete data;
        _currentState = stateIEventMap[_currentState][EVENT_QUEUE.front().event].NewState;
        EVENT_QUEUE.pop();  //delete event struct from queue
        
    }
}