#include <mutex>
#include <queue>

/** SAFE_QUEUE is a queue that is safe to use for multithreaded
 * applications. It locks and unlocks it's resources as needed
 * to ensure thread safe operation. */
template <class T>
class SAFE_QUEUE //this is a thread safe queue
{
  private:
    std::mutex resourceControl;
    std::queue<T> queue;

  public:
    size_t size()
    {
        size_t size;
        resourceControl.lock();
        size = queue.size();
        resourceControl.unlock();
        return size;
    }
    T front()
    {
        resourceControl.lock();
        T item = queue.front();
        resourceControl.unlock();
        return item;
    }
    void pop()
    {
        resourceControl.lock();
        queue.pop();
        resourceControl.unlock();
    }
    void push(T item)
    {
        resourceControl.lock();
        queue.push(item);
        resourceControl.unlock();
    }
    bool empty()
    {
        resourceControl.lock();
        bool isEmpty = queue.empty();
        resourceControl.unlock();
        return isEmpty;
    }
};